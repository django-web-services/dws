import React, { Component } from 'react';
import axios from 'axios';

class App extends Component {
  constructor() {
    super()
    this.state = {
      exchangeData: null
    }
  }
  componentDidMount() {
    axios.get('http://api.fixer.io/latest?base=USD&symbols=TRY').then(resp => {
      if (!resp.data.rates) return false;
      this.setState({
        exchangeData: resp.data
      })
    })
  }
  render() {
    return (
      <div className="container">
        <h3>DWS Dashboard</h3>
        {this.state.exchangeData ? 
        (<p>{this.state.exchangeData.date} itibariyle 1 USD = {this.state.exchangeData.rates.TRY} TL :)))</p>)
        : "-"}
      </div>
    );
  }
}

export default App;
